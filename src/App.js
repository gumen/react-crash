import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Header from './components/layout/Header'
import AddTodo from './components/AddTodo'
import Todos from './components/Todos'
import About from './components/pages/About'

import './App.css'

class App extends React.Component {
  state = {
    todos: []
  }

  componentDidMount() {
    fetch('http://jsonplaceholder.typicode.com/todos?_limit=10')
      .then(res => {
        if (res.ok) return res.json()
        else throw(res)
      })
      .then(data => {
        this.setState({ todos: data })
      })
      .catch(error => {
        console.error('fetch error', error)
      })
  }

  toggleComplete = (id) => {
    this.setState({
      todos: this.state.todos.map(todo => {
        if(todo.id === id) todo.completed = !todo.completed
        return todo
      })
    })
  }

  deleteTodo = (id) => {
    fetch('https://jsonplaceholder.typicode.com/todos/' + id, {
      method: 'DELETE',
    })
      .then(res => {
        if (res.ok) {
          this.setState({
            todos: this.state.todos.filter(todo => todo.id !== id)
          })
        } else {
          throw(res)
        }
      })
      .catch(error => {
        console.error('addTodo fetch post error', error)
      })
  }

  addTodo = (title) => {
    fetch('https://jsonplaceholder.typicode.com/todos', {
      method: 'POST',
      headers: {'Content-type': 'application/json; charset=UTF-8'},
      body: JSON.stringify({ title, completed: false })
    })
      .then(res => {
        console.log('res', res)
        if (res.ok) return res.json()
        else throw(res)
      })
      .then(data => {
        this.setState({ todos: [...this.state.todos, data] })
      })
      .catch(error => {
        console.error('addTodo fetch post error', error)
      })
  }

  render() {
    return (
      <Router>
        <div className="App">
          <div className="container">
            <Header />
            <Route
              exact
              path="/"
              render={props => (
                <React.Fragment>
                  <AddTodo
                    addTodo={this.addTodo}
                  />
                  <Todos
                    todos={this.state.todos}
                    toggleComplete={this.toggleComplete}
                    deleteTodo={this.deleteTodo}
                  />
                </React.Fragment>
              )}
            />
            <Route
              path="/about"
              component={About}
            />
          </div>
        </div>
      </Router>
    )
  }
}

export default App
