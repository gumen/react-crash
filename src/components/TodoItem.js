import React from 'react'
import PropTypes from 'prop-types'

const btnStyle = {
  backgroundColor: '#ff0000',
  color: '#fff',
  border: '0',
  padding: '5px 10px',
  borderRadius: '50%',
  cursor: 'pointer',
  float: 'right',
}

class TodoItem extends React.Component {
  getStyle() {
    return {
      backgroundColor: '#f4f4f4',
      padding: '10px',
      borderBottom: '1px #ccc dotted',
      textDecoration: this.props.todo.completed ? 'line-through' : 'none',
    }
  }

  toggleComplete() {
    console.log(this.props)
  }

  render() {
    const {id, title, completed} = this.props.todo;

    return (
      <div style={this.getStyle()}>
        <p>
          <input
            defaultChecked={completed}
            type="checkbox"
            onChange={this.props.toggleComplete.bind(null, id)}
          />
          {' '}
          {title}
          <button
            style={btnStyle}
            onClick={this.props.deleteTodo.bind(null, id)}
          >X</button>
        </p>
      </div>
    )
  }
}

TodoItem.propTypes = {
  todo           : PropTypes.object.isRequired,
  toggleComplete : PropTypes.func.isRequired,
  deleteTodo     : PropTypes.func.isRequired,
}

export default TodoItem
